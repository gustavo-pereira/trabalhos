package escalonador;

public class Arquivo {
	private Integer id = Utils.getRandom();
	private Integer duracao;
	private Integer tamanho;
	
	public Arquivo(Integer duracao, Integer tamanho) {
		this.duracao = duracao;
		this.tamanho = tamanho;
	}

	public Integer getId() {
		return id;
	}

	public Integer getDuracao() {
		return duracao;
	}

	public void setDuracao(Integer duracao) {
		this.duracao = duracao;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}
}
