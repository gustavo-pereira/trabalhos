package escalonador;

import java.util.Vector;

public class CircularList {

	private Vector<Thread> list;
	private int index;

	public CircularList() {
		list = new Vector<>(10);
		index = 0;
	}

	/**
	 * this method returns the next element in the list.
	 * @return Object
	 */
	public Thread getNext() {
		Thread nextElement = null;
		if (!list.isEmpty()) {
			if (index == list.size())
				index = 0;
			nextElement = list.elementAt(index);
			++index;
		}
		return nextElement;
	}

	/**
	 * this method adds an item to the list
	 * @return void
	 */
	public void addItem(Thread t) {
		list.addElement(t);
	}
}