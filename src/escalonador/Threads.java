package escalonador;

public class Threads {
	public static void main(String[] args) {
		for (int i = 0; i < 3; i++) {
			new Thread(() -> {
				System.out.println("Thread "+ Utils.getRandom());
			}).start();
		}
		new Thread(() -> {
			System.out.println("Thread 2");
		}).start();
		new Thread(() -> {
			System.out.println("Thread 3");
		}).start();
	}
}
